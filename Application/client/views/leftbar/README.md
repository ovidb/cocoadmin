# The LeftBar Module

Name is self explenatory but features aren't. Here is my take on them:

* Search form
* Current user's profile with a few actions
* Multiple level collapsible items with icons
* The whole thing should be able to collapse from icon and text to icon only
* Footer-Bar for showing miscellaneous information
