Template.mainLayout.rendered = ->
  FastClick.attach document.body
  resizefunc.push "initscrolls"
  resizefunc.push "changeptype"
  $(".sparkline").sparkline "html",
    enableTagOptions: true

  $(".animate-number").each ->
    $(this).animateNumbers $(this).attr("data-value"), true, parseInt($(this).attr("data-duration"))
    return

  
  #TOOLTIP
  $("body").tooltip
    selector: "[data-toggle=tooltip]"
    container: "body"

  
  #RESPONSIVE SIDEBAR
  $(".open-right").click (e) ->
    $("#wrapper").toggleClass "open-right-sidebar"
    e.stopPropagation()
    $("body").trigger "resize"
    return

  $(".open-left").click (e) ->
    e.stopPropagation()
    $("#wrapper").toggleClass "enlarged"
    $("#wrapper").addClass "forced"
    if $("#wrapper").hasClass("enlarged") and $("body").hasClass("fixed-left")
      $("body").removeClass("fixed-left").addClass "fixed-left-void"
    else $("body").removeClass("fixed-left-void").addClass "fixed-left"  if not $("#wrapper").hasClass("enlarged") and $("body").hasClass("fixed-left-void")
    if $("#wrapper").hasClass("enlarged")
      $(".left ul").removeAttr "style"
    else
      $(".subdrop").siblings("ul:first").show()
    toggle_slimscroll ".slimscrollleft"
    $("body").trigger "resize"
    return

  
  # LEFT SIDE MAIN NAVIGATION
  $("#sidebar-menu a").on "click", (e) ->
    unless $("#wrapper").hasClass("enlarged")
      e.preventDefault()  if $(this).parent().hasClass("has_sub")
      unless $(this).hasClass("subdrop")
        
        # hide any open menus and remove all other classes
        $("ul", $(this).parents("ul:first")).slideUp 350
        $("a", $(this).parents("ul:first")).removeClass "subdrop"
        $("#sidebar-menu .pull-right i").removeClass("fa-angle-up").addClass "fa-angle-down"
        
        # open our new menu and add the open class
        $(this).next("ul").slideDown 350
        $(this).addClass "subdrop"
        $(".pull-right i", $(this).parents(".has_sub:last")).removeClass("fa-angle-down").addClass "fa-angle-up"
        $(".pull-right i", $(this).siblings("ul")).removeClass("fa-angle-up").addClass "fa-angle-down"
      else if $(this).hasClass("subdrop")
        $(this).removeClass "subdrop"
        $(this).next("ul").slideUp 350
        $(".pull-right i", $(this).parent()).removeClass("fa-angle-up").addClass "fa-angle-down"
    return

  
  #$(".pull-right i",$(this).parents("ul:eq(1)")).removeClass("fa-chevron-down").addClass("fa-chevron-left");
  
  # NAVIGATION HIGHLIGHT & OPEN PARENT
  $("#sidebar-menu ul li.has_sub a.active").parents("li:last").children("a:first").addClass("active").trigger "click"
  
  #WIDGET ACTIONS
  $(".widget-header .widget-close").on "click", (event) ->
    event.preventDefault()
    $item = $(this).parents(".widget:first")
    bootbox.confirm "Are you sure to remove this widget?", (result) ->
      if result is true
        $item.addClass "animated bounceOutUp"
        window.setTimeout (->
          if $item.data("is-app")
            $item.removeClass "animated bounceOutUp"
            $item.find(".widget-popout").click()  if $item.hasClass("ui-draggable")
            $item.hide()
            $("a[data-app='" + $item.attr("id") + "']").addClass "clickable"
          else
            $item.remove()
          return
        ), 300
      return

    return

  $(document).on "click", ".widget-header .widget-toggle", (event) ->
    event.preventDefault()
    $(this).toggleClass("closed").parents(".widget:first").find(".widget-content").slideToggle()
    return

  $(document).on "click", ".widget-header .widget-popout", (event) ->
    event.preventDefault()
    widget = $(this).parents(".widget:first")
    if widget.hasClass("modal-widget")
      $("i", this).removeClass("icon-window").addClass "icon-publish"
      widget.removeAttr("style").removeClass "modal-widget"
      widget.find(".widget-maximize,.widget-toggle").removeClass "nevershow"
      widget.draggable("destroy").resizable "destroy"
    else
      widget.removeClass "maximized"
      widget.find(".widget-maximize,.widget-toggle").addClass "nevershow"
      $("i", this).removeClass("icon-publish").addClass "icon-window"
      w = widget.width()
      h = widget.height()
      widget.addClass("modal-widget").removeAttr("style").width(w).height h
      $(widget).draggable(
        handle: ".widget-header"
        containment: ".content-page"
      ).css(
        left: widget.position().left - 2
        top: widget.position().top - 2
      ).resizable
        minHeight: 150
        minWidth: 200

    window.setTimeout (->
      $("body").trigger "resize"
      return
    ), 300
    return

  $("a[data-app]").each (e) ->
    app = $(this).data("app")
    status = $(this).data("status")
    $("#" + app).data "is-app", true
    if status is "inactive"
      $("#" + app).hide()
      $(this).addClass "clickable"
    return

  $(document).on "click", "a[data-app].clickable", (event) ->
    event.preventDefault()
    $(this).removeClass "clickable"
    app = $(this).data("app")
    $("#" + app).show()
    $("#" + app + " .widget-popout").click()
    topd = $("#" + app).offset().top - $(window).scrollTop()
    $("#" + app).css(
      left: "10"
      top: -(topd - 60) + "px"
    ).addClass "fadeInDown animated"
    window.setTimeout (->
      $("#" + app).removeClass "fadeInDown animated"
      return
    ), 300
    return

  $(document).on "click", ".widget", ->
    if $(this).hasClass("modal-widget")
      $(".modal-widget").css "z-index", 5
      $(this).css "z-index", 6
    return

  $(document).on "click", ".widget .reload", (event) ->
    event.preventDefault()
    el = $(this).parents(".widget:first")
    blockUI el
    window.setTimeout (->
      unblockUI el
      return
    ), 1000
    return

  $(document).on "click", ".widget-header .widget-maximize", (event) ->
    event.preventDefault()
    $(this).parents(".widget:first").removeAttr("style").toggleClass "maximized"
    $("i", this).toggleClass("icon-resize-full-1").toggleClass "icon-resize-small-1"
    $(this).parents(".widget:first").find(".widget-toggle").toggleClass "nevershow"
    $("body").trigger "resize"
    false

  #RUN RESIZE ITEMS
  $(window).resize debounce(resizeitems, 100)
  $("body").trigger "resize"
  
  #SELECT
  #$(".selectpicker").selectpicker()
  
  #FILE INPUT
  $("input[type=file]").bootstrapFileInput()
  
  #DATE PICKER
  $(".datepicker-input").datepicker()
  
  #ICHECK
  $("input:not(.ios-switch)").iCheck
    checkboxClass: "icheckbox_square-aero"
    radioClass: "iradio_square-aero"
    increaseArea: "20%" # optional

  
  # IOS7 SWITCH
  $(".ios-switch").each ->
    mySwitch = new Switch(this)
    return

  
  #GALLERY
  $(".gallery-wrap").each -> # the containers for all your galleries
    $(this).magnificPopup
      delegate: "a.zooming" # the selector for gallery item
      type: "image"
      removalDelay: 300
      mainClass: "mfp-fade"
      gallery:
        enabled: true

    return

  return