templates = {}

@getTemplate = (name)->
  # if tamplate has been overwriten return this, else return template name
  if !!templates[name] then templates[name] else name;

Router.setTemplateNameConverter (str)-> str

@_functionRunned = {}
@TrackFunctionRuns = {}
TrackFunctionRuns.increment = (fn)->
    if typeof _functionRunned[fn] isnt 'undefined'
      _functionRunned[fn]++
    else
      _functionRunned[fn] = 1
    console.log _functionRunned[fn]
    return
TrackFunctionRuns.reset = (fn)->
  _functionRunned[fn] = undefined
  return
TrackFunctionRuns.show = (fn)->
  _functionRunned[fn] ? "Function #{fn} has runned #{_functionRunned[fn]} times"

TrackFunctionRuns.showAll = ()->
  _functionRunned